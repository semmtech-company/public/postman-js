# Postman JavaScripts

This repository is intended to expose public JavaScript libraries, in order to directly use (or evaluate) them as part of Postman pre-request scripts.

## Usage

First you should perform a GET request (once per library) for the specified URL. In case of the jsrsasign library, this will be [https://gitlab.com/semmtech-company/public/postman-js/-/raw/master/jsrsasign/jsrsasign-8.0.6-min-postman.js](https://gitlab.com/semmtech-company/public/postman-js/-/raw/master/jsrsasign/jsrsasign-8.0.6-min-postman.js). Also you should assign the `responseBody` to a global variable within the Tests of your Postman request (see screenshot):

```javascript
// Store the whole JavaScript source within a global variable called 'jsrsasign'
postman.setGlobalVariable('jsrsasign', responseBody);
```

![install-jsrsasign](doc/images/install-jsrsasign.png)

In order to include one of the above libraries within your pre-request, simply run an `eval()` with the global variable set during installation above. Like

```javascript
// Evaluate the script sources stored in the global variable 'jsrsasign', in order to load it into the Pre-request Script
eval(globals.jsrsasign);
```

![use-jsrsasign](doc/images/use-jsrsasign.png)

## Libraries

The follwing JS libraries are avaialble in this repository:

* [jsrsasign 8.0.6](#jsrsasign)


### <a name="jsrsasign"></a>jsrsasign 8.0.6

The jsrsasign'(RSA-Sign JavaScript Library) is an opensource free cryptography library supporting RSA/RSAPSS/ECDSA/DSA signing/validation, ASN.1, PKCS#1/5/8 private/public key, X.509 certificate, CRL, OCSP, CMS SignedData, TimeStamp, CAdES JSON Web Signature/Token in pure JavaScript.

See [https://github.com/kjur/jsrsasign](https://github.com/kjur/jsrsasign) for more details.

In order to include this library use the following URL: 

```
https://gitlab.com/semmtech-company/public/postman-js/-/raw/master/jsrsasign/jsrsasign-8.0.6-min-postman.js
```

For more information about signing and verifying signatures using this libraray, check out [Tutorial for Signature class](https://github.com/kjur/jsrsasign/wiki/Tutorial-for-Signature-class)

The file [example-postman-collection.json](jsrassign/example-postman-collection.json) contains a Postman collection (v2.1) consisting of two requests. 

One to install jsrsasign library into global variable: 

![install-jsrsasign](doc/images/jsrsasign-installation.png)

Another to use the library to sign and verify a message. This request uses the Postman Echo service and sends two parameters to the service `message` and `signature`:

![signature-checking-params](doc/images/jsrsasign-signature-checking-params.png)

The Pre-request script shows how to sign the message using a private key:

![signature-checking-pre-request](doc/images/jsrsasign-signature-checking-pre-request.png)

Whereas the Tests show how to retrieve the signature and verify it using the paired public key:

![signature-checking-tests](doc/images/jsrsasign-signature-checking-tests.png)



